#!/bin/bash -xe

SRC_DIR=$PWD

mkdir build
cd build

qmake $SRC_DIR
make -j$(nproc)
make install INSTALL_ROOT=$PWD/install/

SIGNING_ARGS=""
if [[ "$KEYPASS" ]]; then
    SIGNING_ARGS="--sign $KEYSTORE $KEYALIAS --keypass $KEYPASS --storepass $KEYPASS"
fi

androiddeployqt --input example/*.json --output $PWD/install/ $SIGNING_ARGS
